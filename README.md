# Projet-COON

## Présentation du projet
    
Ce projet à été effectué au cours de la matière COON en 2022.\
Dans le cadre de ce projet, a été conçu un prototype de veilleuse automatique pouvant jouer des sons.\
Ce projet est un projet arduino.

## Librairies

4 librairies différentes ont été utilisées pour ce projet.\

1 pour la gestion d'une bande de LED :

- [FastLED](https://github.com/FastLED/FastLED)

3 pour la gestion de la musique :

- [Melody](https://github.com/dualB/Melody)
- [Musician](https://github.com/dualB/Musician)
- [Mozzi](https://github.com/sensorium/Mozzi)

## Software


## Hardware

Voici une liste de tous les composants utilisés :

- Un senseur ultra-sons utilisé comme détecteur pour changer la musique 
- un potentiomètre utilisé comme résistance modulaire et permettant de varier le son de la musique
- une bande à LED pour la lumière de la veilleuse
- une mini photodiode pour détecter la lumiere ambiante
- un haut-parleur pour jouer les musiques

